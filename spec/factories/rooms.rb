FactoryBot.define do
  factory :room do
    slots { rand(1..10) }
    name { Faker::Color.color_name.capitalize }

    factory :full_room do
      after(:create) do |room, evaluator|
        create_list(:room_slot, room.slots, room: room)
        room.reload
      end
    end

    factory :almost_full_room do
      after(:create) do |room, evaluator|
        create_list(:room_slot, room.slots-1, room: room)
        room.reload
      end
    end
  end
end
