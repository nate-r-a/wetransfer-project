require 'rails_helper'

RSpec.describe "/rooms", type: :request do
  let(:user) { create :user }
  let!(:open_room) { create :room }
  let!(:full_room) { create :full_room }

  describe "GET /open" do
    it "should only show the open room" do
      get "/api/v1/rooms/open?user_id=#{user.id}", as: :json
      expect(JSON.parse(response.body).dig("data").length).to eq(1)
    end
  end

  describe "POST /join" do
    it "should join the open room" do
      post "/api/v1/rooms/#{open_room.id}/join/#{user.id}", as: :json
      expect(JSON.parse(response.body).dig("data", "id")).to be_truthy
    end

    it "should not join the full room" do
      post "/api/v1/rooms/#{full_room.id}/join/#{user.id}", as: :json
      expect(JSON.parse(response.body).dig("data", "id")).to be_falsey
    end
  end
end
