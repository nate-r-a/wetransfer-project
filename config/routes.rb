Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :rooms, only: :index do
        collection do
          get 'open'
        end

        member do
          post 'join/:user_id', action: 'join'
        end
      end
    end
  end
end
