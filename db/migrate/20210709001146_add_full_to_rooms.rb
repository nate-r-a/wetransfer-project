class AddFullToRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :full, :boolean, default: false
  end
end
