This small app allows a user to view open rooms and book space in them.

The seed file will create some starter data, but FactoryBot can also be used to make rooms and users manually:
```
u = FactoryBot.create(:user)
r = FactoryBot.create(:room)
FactoryBot.create(:room_slot, room: r, user: u)
```

To view open rooms for User with ID 1:
`api/v1/rooms/open?user_id=1`

To have User with ID 1 join open Room with ID 5:
`api/v1/rooms/5/join/1`

#### Notes
- There are checks to stop a user from joining either a full room, or a room that is already full.
- If I could start this again, I would probably do it with GraphQL (though it's not exactly RESTful). It has a pretty intense setup process, but in my experience it offers a lot more flexibility on both the front and back end, and has matured a lot over the past 2 years or so.
- This uses `jsonapi-serializer` to create the JSON objects but again, GraphQL could have solved some issues here.
- With more time:
    - I'd like to add Devise or a proper authentication system.
        - This app currently just relies on the front end passing in the user's ID as a param, which is not particularly secure.
    - I'd also add ActiveStorage images to the Room model.
    - Expand on the testing suite. 
    - I used scaffolding to set up the initial models and controllers, but ended up not using many of them. I would clean up the codebase to eliminate unused code.
