class RoomSlotSerializer
  include JSONAPI::Serializer

  belongs_to :room
  belongs_to :user
end
