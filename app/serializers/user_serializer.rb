class UserSerializer
  include JSONAPI::Serializer

  attributes :name
  
  has_many :room_slots
end
