class RoomSerializer
  include JSONAPI::Serializer

  attributes :name, :slots, :available_slots

  has_many :room_slots
end
