class Api::V1::RoomsController < ApplicationController
  before_action :set_room, only: [:show, :update, :destroy, :join]
  before_action :set_user, only: [:open, :join]

  # GET /rooms
  def index
    @rooms = Room.all

    render json: RoomSerializer.new(@rooms)
  end

  # GET /open
  def open
    @rooms = @user.open_rooms

    render json: RoomSerializer.new(@rooms)
  end

  def join
    @room_slot = @room.room_slots.build({user: @user})

    if @room_slot.save
      render json: RoomSlotSerializer.new(@room_slot)
    else
      render json: @room_slot.errors, status: :bad_request
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    def set_user
      @user = User.find(params[:user_id])
    end
end
