class RoomSlot < ApplicationRecord
  belongs_to :room
  belongs_to :user

  validate :room_is_available_to_join
  private

  def room_is_available_to_join
    # Check is user is already in room
    if RoomSlot.where(room: room, user: user).exists?
      errors.add(:user, "is already in room")
      return false
    end

    # Check if room is already full
    room_slot_count = room.room_slots.count
    if room_slot_count >= room.slots
      errors.add(:room, "is already full")
      return false
    else
      # Update full status when difference is 1 because the 
      # room slot will be added right after this block
      room.update(full: true) if (room.slots-room_slot_count) == 1
    end
  end
end
