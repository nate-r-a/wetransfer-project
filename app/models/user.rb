class User < ApplicationRecord
  has_many :room_slots, dependent: :destroy
  has_many :rooms, through: :room_slots

  def open_rooms
    # Note: returns an Array
    Room.not_full - rooms
  end
end
