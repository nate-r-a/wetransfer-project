class Room < ApplicationRecord
  has_many :room_slots, dependent: :destroy
  has_many :users, through: :room_slots

  scope :full, -> { where(full: true) }
  scope :not_full, -> { where(full: false) }

  def available_slots
    slots - room_slots.count
  end
end
